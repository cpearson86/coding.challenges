# Hex to RGB Color Challenge

The purpose of this challenge is to write a simple program that converts the hexadecimal color format (like `#FFFFFF`) into an RGB decimal code (like `rgb(255, 255, 255)`).