Periodically, I like to take on coding challenges to test and expand my software development skills. This repository contains some of my solutions for those challenges.

Each folder contains a Markdown file with a description of the challenge, along with 1 or more source files containing my solution for the given challenge (plus, any other relevant data files).

I've written solutions in the following languages:

* PHP
* Ruby
* JavaScript
* Python
* Go
* LOLCODE
* PostScript (hand-written)
* SVG (hand-written)