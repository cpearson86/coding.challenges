<?php

class Solution {
    function wordPattern($pattern, $str) {
        $letterIndex = [];
        $wordIndex = [];
        
        $letters = array_filter(str_split($pattern));
        $words = array_filter(explode(' ', $str));
        
        if (count($letters) != count($words)) {
            return false;
        }
        
        foreach ($letters as $key => $letter) {
            if (array_key_exists($letter, $letterIndex)) {
                if ($letterIndex[$letter] != $words[$key]) {
                    return false;
                }
            } else {
                $letterIndex[$letter] = $words[$key];
            }
            
            if (array_key_exists($words[$key], $wordIndex)) {
                if ($wordIndex[$words[$key]] !== $letter) {
                    return false;
                }
            } else {
                $wordIndex[$words[$key]] = $letter;
            }
        }
        
        return true;
    }
}